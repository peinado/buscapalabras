#!/usr/bin/env pythoh3

'''
Busca una palabra en una lista de palabras
'''

import sortwords
import sys
def search_word(word, words_list):
    test_index = 0
    try:
        for x in range (1, len(words_list)):
            if sortwords.equal(word, words_list[x]):
                test_index = x
    except:
        Exception

    return test_index


def main():

    ordered_list = sortwords.sort(sys.argv[2:])
    position = search_word(sys.argv[1], ordered_list)
    sortwords.show(ordered_list)
    print(position)


if __name__ == '__main__':
    main()
